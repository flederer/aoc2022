task1(MostCalories) :-
  most_calories(_, MostCalories).

task2(Calories) :-
  top_most_calories(3, MostCalories),
  sum_list(MostCalories, Calories).

total_elf_calories(Elf, TotalCalories) :-
  elf(Elf),
  aggregate(
    sum(Calories),
    elf_calories(Elf, Calories),
    TotalCalories).

most_calories(ElfWithMostCalories, MostCalories) :-
  most_calories(ElfWithMostCalories, MostCalories, []).

most_calories(ElfWithMostCalories, MostCalories, ExcludedElves) :-
  total_elf_calories(ElfWithMostCalories, MostCalories),
  \+ member(ElfWithMostCalories, ExcludedElves),
  \+ (total_elf_calories(Elf, OtherCalories),
      \+ member(Elf, ExcludedElves),
      OtherCalories > MostCalories),
  !.

top_most_calories(TopNumber, MostCalories) :-
  top_most_calories(TopNumber, [], MostCalories).

top_most_calories(0, _, []) :- !.

top_most_calories(TopNumber, ExcludedElves, [MostCalories|NextMostCalories]) :-
  most_calories(ElfWithMostCalories, MostCalories, ExcludedElves),
  NextTopNumber is TopNumber - 1,
  top_most_calories(NextTopNumber, [ElfWithMostCalories|ExcludedElves], NextMostCalories).


%--------------------------------------------------

assert_inputs :-
  retractall(elf(_)),
  retractall(elf_calories(_, _)),
  file_lines(input, Lines),
  assert_inputs(Lines, 1).

assert_inputs([], _) :- !.

assert_inputs([""|Lines], Elf) :-
  !,
  assertz(elf(Elf)),
  NextElf is Elf + 1,
  assert_inputs(Lines, NextElf).

assert_inputs([Line|Lines], Elf) :-
  atom_number(Line, Calories),
  assertz(elf_calories(Elf, Calories)),
  assert_inputs(Lines, Elf).

file_lines(File, Lines) :-
  setup_call_cleanup(
    open(File, read, Stream),
    ( read_string(Stream, _, Content),
      split_string(Content, '\n', '', Lines)),
    close(Stream)).

:- assert_inputs.
