task1(TotalSize) :-
  aggregate_all(
    sum(Size),
    directory_with_max_size(100000, _, Size),
    TotalSize).

task2(Size) :-
  size_to_free(SizeToFree),
  directory_size(Path, Size),
  Size >= SizeToFree,
  \+ (  directory_size(OtherPath, OtherSize),
        OtherSize >= SizeToFree,
        OtherSize < Size,
        OtherPath \= Path),
  writeln(Path),
  !.

size_to_free(SizeToFree) :-
  unused_size(UnusedSize),
  required_unused_size(RequiredSize),
  SizeToFree is RequiredSize - UnusedSize.

unused_size(UnusedSize) :-
  directory_size('', UsedSize),
  filesystem_size(FilesystemSize),
  UnusedSize is FilesystemSize - UsedSize.

directory_with_max_size(MaxSize, Path, Size) :-
  directory_size(Path, Size),
  Size =< MaxSize.

directory_size(Path, TotalSize) :-
  directory(Path, Subdirectories, Files),
  aggregate_all(
    sum(Size),
    ( member(Subdirectory, Subdirectories),
      directory_size(Subdirectory, Size)),
    SubdirectoriesSize),
  aggregate_all(
    sum(Size),
    member(file(_, Size), Files),
    FilesSize),
  TotalSize is SubdirectoriesSize + FilesSize.

filesystem_size(70000000).

required_unused_size(30000000).

%--------------------------------------------------

assert_directories :-
  retractall(directory(_, _, _)),
  input([_|Input]),
  assert_directories(Input, '').

assert_directories([[$, cd, ..]|Entries], Path/_) :-
  !,
  assert_directories(Entries, Path).

assert_directories([[$, cd, Directory]|Entries], Path) :-
  !,
  assert_directories(Entries, Path/Directory).

assert_directories([[$, ls]|Entries], Path) :-
  !,
  subdirectories(Entries, Path, Subdirectories),
  files(Entries, Path, Files),
  assertz(directory(Path, Subdirectories, Files)),
  assert_directories(Entries, Path).

assert_directories([['']], _) :- !.

assert_directories([_|Entries], Path) :-
  assert_directories(Entries, Path).

subdirectories([[dir, Directory]|Entries], Path, [Path/Directory|Subdirectories]) :-
  !,
  subdirectories(Entries, Path, Subdirectories).

subdirectories([[$|_]|_], _, []) :- !.

subdirectories([['']], _, []) :- !.

subdirectories([_|Entries], Path, Subdirectories) :-
  subdirectories(Entries, Path, Subdirectories).

files([[SizeAtom, File]|Entries], Path, [file(Path/File, Size)|Files]) :-
  atom_number(SizeAtom, Size),
  !,
  files(Entries, Path, Files).

files([[$|_]|_], _, []) :- !.

files([['']], _, []) :- !.

files([_|Entries], Path, Files) :-
  files(Entries, Path, Files).

%--------------------------------------------------

assert_input :-
  retractall(input(_)),
  setup_call_cleanup(
    open(input, read, Stream),
    read_string(Stream, _, Content),
    close(Stream)),
  split_string(Content, '\n', '', StringLines),
  findall(
    Elements,
    ( member(Line, StringLines),
      split_line(Line, Elements)),
    Lines),
  assertz(input(Lines)).

split_line(Line, Atoms) :-
  split_string(Line, ' ', '', Strings),
  maplist(atom_string, Atoms, Strings).

:- assert_input.
:- assert_directories.
