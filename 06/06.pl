% totally not optimized... but I don't have time now

task1(Position) :-
  disjunct_position(4, Position).

task2(Position) :-
  disjunct_position(14, Position).

disjunct_position(SelectionLength, Position) :-
  input(Chars),
  disjunct_position(SelectionLength, Chars, SelectionLength, Position).

disjunct_position(SelectionLength, Chars, Position, Position) :-
  append(Selection, _,  Chars),
  length(Selection, SelectionLength),
  list_to_set(Selection, Selection),
  !.

disjunct_position(SelectionLength, [_|Chars], Position, ResultPosition) :-
  NextPosition is Position + 1,
  disjunct_position(SelectionLength, Chars, NextPosition, ResultPosition).

%--------------------------------------------------

assert_input :-
  retractall(input(_)),
  setup_call_cleanup(
    open(input, read, Stream),
    read_string(Stream, _, Content),
    close(Stream)),
  split_string(Content, '\n', '', [Line|_]),
  string_chars(Line, Chars),
  assertz(input(Chars)).

:- assert_input.
