task1(NumberOfVisibleTrees) :-
  aggregate_all(
    count,
    visible_tree((_, _)),
    NumberOfVisibleTrees).

task2(MaxScenicScore) :-
  aggregate_all(
    max(ScenicScore),
    tree_scenic_score(_, ScenicScore),
    MaxScenicScore).

visible_tree(Coordinate) :-
  tree(Coordinate, _),
  once(
    tree_visible(left_trees, Coordinate)
  ; tree_visible(right_trees, Coordinate)
  ; tree_visible(up_trees, Coordinate)
  ; tree_visible(down_trees, Coordinate)).

tree_visible(Predicate, Coordinate) :-
  call(Predicate, Coordinate, OtherTrees),
  tree(Coordinate, Height),
  forall(
    member(tree(_, OtherHeight), OtherTrees),
    OtherHeight < Height).

tree_scenic_score(Coordinate, Score) :-
  tree(Coordinate, _),
  tree_sight(left_trees, Coordinate, LeftSight),
  tree_sight(right_trees, Coordinate, RightSight),
  tree_sight(up_trees, Coordinate, UpSight),
  tree_sight(down_trees, Coordinate, DownSight),
  Score is LeftSight * RightSight * UpSight * DownSight.

tree_sight(Predicate, Coordinate, Sight) :-
  call(Predicate, Coordinate, OtherTrees),
  tree(Coordinate, Height),
  tree_sight_(Height, OtherTrees, Sight).

tree_sight_(Height, [tree(_, OtherHeight)|_], 1) :-
  OtherHeight >= Height,
  !.

tree_sight_(Height, [_|Trees], Sight) :-
  !,
  tree_sight_(Height, Trees, PreviousSight),
  Sight is PreviousSight + 1.

tree_sight_(_, [], 0).

left_trees(Coordinate, Trees) :-
  row_trees(-, Coordinate, Trees).

right_trees(Coordinate, Trees) :-
  row_trees(+, Coordinate, Trees).

up_trees(Coordinate, Trees) :-
  column_trees(-, Coordinate, Trees).

down_trees(Coordinate, Trees) :-
  column_trees(+, Coordinate, Trees).

row_trees(Direction, (Row, Column), [tree((Row, NextColumn), Height) | Trees]) :-
  Calculation =.. [Direction, Column, 1],
  NextColumn is Calculation,
  tree((Row, NextColumn), Height),
  !,
  row_trees(Direction, (Row, NextColumn), Trees).

row_trees(_, _, []).

column_trees(Direction, (Row, Column), [tree((NextRow, Column), Height) | Trees]) :-
  Calculation =.. [Direction, Row, 1],
  NextRow is Calculation,
  tree((NextRow, Column), Height),
  !,
  column_trees(Direction, (NextRow, Column), Trees).

column_trees(_, _, []).

%--------------------------------------------------

assert_input :-
  retractall(tree(_, _)),
  setup_call_cleanup(
    open(input, read, Stream),
    read_string(Stream, _, InputsString),
    close(Stream)),
  string_chars(InputsString, Inputs),
  assert_tree((1, 1), Inputs).

assert_tree((Row, Column), ['\n']) :-
  !,
  assert(max_coordinate((Row, Column))).

assert_tree((Row, _), ['\n'|Inputs]) :-
  !,
  NextRow is Row + 1,
  assert_tree((NextRow, 1), Inputs).

assert_tree((Row, Column), [Input|Inputs]) :-
  atom_number(Input, Height),
  assertz(tree((Row, Column), Height)),
  NextColumn is Column + 1,
  assert_tree((Row, NextColumn), Inputs).

:- assert_input.
