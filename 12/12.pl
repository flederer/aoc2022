task1(Length) :-
  field(SourceCoordinate, 'S'),
  shortest_path(SourceCoordinate, Path),
  length(Path, Length).

% not optimized!
task2(MinLength) :-
  findall(
    Length,
    ( field(SourceCoordinate, a),
      shortest_path(SourceCoordinate, Path),
      length(Path, Length)),
    Lengths),
  min_list(Lengths, MinLength).

% Dijkstra
shortest_path(SourceCoordinate, Path) :-
  empty_assoc(Visited),
  shortest_path(Path, Visited, [SourceCoordinate-SourceCoordinate]).

shortest_path(Path, Visited, [ToVisitCoordinate-_|ToVisits]) :-
  gen_assoc(ToVisitCoordinate, Visited, _),
  !,
  shortest_path(Path, Visited, ToVisits).

shortest_path([ToVisitPredecessor | Predecessors], Visited, [DestinationCoordinate-ToVisitPredecessor|_]) :-
  destination_coordinate(DestinationCoordinate),
  !,
  path(Visited, ToVisitPredecessor, Predecessors).

shortest_path(Path, Visited, [ToVisitCoordinate-ToVisitPredecessor|ToVisits]) :-
  put_assoc(ToVisitCoordinate, Visited, ToVisitPredecessor, NewVisited),
  visit(ToVisitCoordinate, Visited, NextToVisits),
  append(ToVisits, NextToVisits, NewToVisits),
  shortest_path(Path, NewVisited, NewToVisits).

visit(DestinationCoordinate, _, _) :-
  destination_coordinate(DestinationCoordinate),
  !.

visit(SourceCoordinate, Visited, ToVisit) :-
  findall(
    Coordinate-SourceCoordinate,
    ( neighbor_path(SourceCoordinate, Coordinate),
      \+ get_assoc(Coordinate, Visited, _)),
    ToVisit).

path(Visited, Coordinate, []) :-
  gen_assoc(Coordinate, Visited, Coordinate),
  !.

path(Visited, Coordinate, [Predecessor|Predecessors]) :-
  get_assoc(Coordinate, Visited, Predecessor),
  path(Visited, Predecessor, Predecessors).

neighbor(Coordinate1, Coordinate2) :-
  Coordinate1 = (Row, Column1),
  Coordinate2 = (Row, Column2),
  field(Coordinate1, _),
  field(Coordinate2, _),
  1 is abs(Column1 - Column2).

neighbor(Coordinate1, Coordinate2) :-
  Coordinate1 = (Row1, Column),
  Coordinate2 = (Row2, Column),
  field(Coordinate1, _),
  field(Coordinate2, _),
  1 is abs(Row1 - Row2).

neighbor_path(SourceCoordinate, DestinationCoordinate) :-
  neighbor(SourceCoordinate, DestinationCoordinate),
  field(SourceCoordinate, SourceHeight),
  field(DestinationCoordinate, DestinationHeight),
  height_number(SourceHeight, SourceHeightNumber),
  height_number(DestinationHeight, DestinationHeightNumber),
  MaxDestinationHeight is SourceHeightNumber + 1,
  DestinationHeightNumber =< MaxDestinationHeight.

height_number('S', Number) :-
  !,
  char_code(a, Number).

height_number('E', Number) :-
  !,
  char_code(z, Number).

height_number(Height, Number) :-
  char_code(Height, Number).

destination_coordinate(Coordinate) :-
  field(Coordinate, 'E').

%--------------------------------------------------
% Input File Reading
%--------------------------------------------------

assert_input :-
  retractall(field(_, _)),
  file_lines(input, Lines),
  assert_lines(Lines).

assert_lines(Lines) :-
  assert_lines(1, Lines).

assert_lines(Row, [Line|Lines]) :-
  string_chars(Line, Fields),
  assert_fields(Row, 1, Fields),
  NextRow is Row + 1,
  assert_lines(NextRow, Lines).

assert_lines(_, [""]).

assert_fields(Row, Column, [HeightString|Heights]) :-
  atom_string(Height, HeightString),
  assertz(field((Row, Column), Height)),
  NextColumn is Column + 1,
  assert_fields(Row, NextColumn, Heights).

assert_fields(_, _, []).

file_lines(File, Lines) :-
  setup_call_cleanup(
    open(File, read, Stream),
    ( read_string(Stream, _, Content),
      split_string(Content, '\n', '', Lines)),
    close(Stream)).

:- assert_input.
