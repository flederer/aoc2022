task1(Priorities) :-
  aggregate_all(
    sum(Priority),
    ( same_item_in_compartments(Item),
      item_priority(Item, Priority)),
    Priorities).

task2(Priorities) :-
  aggregate_all(
    sum(Priority),
    ( group_badge(Badge),
      item_priority(Badge, Priority)),
    Priorities).

rucksack(Group, Items) :-
  input(Group, Input),
  atom_chars(Input, Items).

compartments(First, Second) :-
  rucksack(_, Items),
  length(Items, NumberOfItems),
  CompartmentLength is NumberOfItems / 2,
  append(First, Second, Items),
  length(First, CompartmentLength).

same_item_in_compartments(Item) :-
  compartments(FirstCompartment, SecondCompartment),
  same_item_in_compartments(FirstCompartment, SecondCompartment, Item).

same_item_in_compartments(FirstCompartment, SecondCompartment, Item) :-
  member(Item, FirstCompartment),
  member(Item, SecondCompartment),
  !.

item_priority(Item, Priority) :-
  char_code(a, LowercaseAAsciiCode),
  char_code(z, LowercaseZAsciiCode),
  char_code(Item, AsciiCode),
  LowercaseAAsciiCode =< AsciiCode,
  AsciiCode =< LowercaseZAsciiCode,
  !,
  Priority is AsciiCode - LowercaseAAsciiCode + 1.

item_priority(Item, Priority) :-
  char_code('A', UppercaseAAsciiCode),
  char_code('Z', UppercaseZAsciiCode),
  char_code(Item, AsciiCode),
  UppercaseAAsciiCode =< AsciiCode,
  AsciiCode =< UppercaseZAsciiCode,
  Priority is AsciiCode - UppercaseAAsciiCode + 27.

group_badge(Badge) :-
  group_rucksacks(_, [Rucksack1, Rucksack2, Rucksack3]),
  intersection(Rucksack1, Rucksack2, Intersection),
  intersection(Intersection, Rucksack3, BadgeList),
  list_to_set(BadgeList, [Badge]).

group_rucksacks(Group, Rucksacks) :-
  group(Group),
  findall(
    Rucksack,
    rucksack(Group, Rucksack),
    Rucksacks).

%--------------------------------------------------

assert_inputs :-
  retractall(input(_, _)),
  retractall(group(_)),
  file_lines(input, Lines),
  assert_line_groups(1, Lines).

assert_line_groups(_, [""]) :- !.

assert_line_groups(Group, [Line1, Line2, Line3|Lines]) :-
  assertz(group(Group)),
  assertz(input(Group, Line1)),
  assertz(input(Group, Line2)),
  assertz(input(Group, Line3)),
  NextGroup is Group + 1,
  assert_line_groups(NextGroup, Lines).

file_lines(File, Lines) :-
  setup_call_cleanup(
    open(File, read, Stream),
    ( read_string(Stream, _, Content),
      split_string(Content, '\n', '', Lines)),
    close(Stream)).

:- assert_inputs.
