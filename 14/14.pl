task1(NumberOfSand) :-
  play1(Sand),
  length(Sand, NumberOfSand).

task2(NumberOfSand) :-
  play2(Sand),
  length(Sand, NumberOfSand).


play1(ResultSand) :-
  sand_source(SandSource),
  play1([], SandSource, ResultSand).

play1(RestingSand, (_, Y), RestingSand) :-
  floor(FloorY),
  MaxY is FloorY - 1,
  Y >= MaxY,
  !.

play1(RestingSand, Sand, ResultSand) :-
  move(RestingSand, Sand, NextSand),
  !,
  play1(RestingSand, NextSand, ResultSand).

play1(RestingSand, MovingSand, [MovingSand|RestingSand]) :-
  sand_source(MovingSand),
  !.

play1(RestingSand, MovingSand, ResultSand) :-
  sand_source(SandSource),
  play1([MovingSand|RestingSand], SandSource, ResultSand).


play2(ResultSand) :-
  sand_source(SandSource),
  play2([], SandSource, ResultSand).

play2(RestingSand, Sand, ResultSand) :-
  move(RestingSand, Sand, NextSand),
  play2(RestingSand, NextSand, ResultSand),
  !.

play2(RestingSand, MovingSand, [MovingSand|RestingSand]) :-
  sand_source(MovingSand),
  !.

play2(RestingSand, MovingSand, ResultSand) :-
  sand_source(SandSource),
  play2([MovingSand|RestingSand], SandSource, ResultSand).


move(RestingSand, (MovingSandX, MovingSandY), NextMovingSand) :-
  NextMovingSandY is MovingSandY + 1,
  NextMovingSand = (MovingSandX, NextMovingSandY),
  \+ blocked_field(NextMovingSand, RestingSand),
  !.

move(RestingSand, (MovingSandX, MovingSandY), NextMovingSand) :-
  NextMovingSandY is MovingSandY + 1,
  NextMovingSandX is MovingSandX - 1,
  NextMovingSand = (NextMovingSandX, NextMovingSandY),
  \+ blocked_field(NextMovingSand, RestingSand),
  !.

move(RestingSand, (MovingSandX, MovingSandY), NextMovingSand) :-
  NextMovingSandY is MovingSandY + 1,
  NextMovingSandX is MovingSandX + 1,
  NextMovingSand = (NextMovingSandX, NextMovingSandY),
  \+ blocked_field(NextMovingSand, RestingSand),
  !.


blocked_field(Coordinate, RestingSand) :- 
  member(Coordinate, RestingSand),
  !.

blocked_field(Coordinate, _) :-
  rock_field(Coordinate),
  !.

blocked_field((_, Y), _) :-
  floor(Y).


sand_source((500, 0)).

%--------------------------------------------------
% Additional Assert
%--------------------------------------------------

assert_floor :-
  retractall(floor(_)),
  rock_field((X, Y)),
  \+ (rock_field((OtherX, OtherY)),
      OtherY > Y,
      X \= OtherX),
  Floor is Y + 2,
  assertz(floor(Floor)).

assert_rock_fields :-
  retractall(rock_field(_)),
  forall(
    ( rock_path(Path),
      append(_, [(X, Y1), (X, Y2) | _], Path),
      directional_between(Y1, Y2, Y)),
    assertz(rock_field((X, Y)))),
  forall(
    ( rock_path(Path),
      append(_, [(X1, Y), (X2, Y) | _], Path),
      directional_between(X1, X2, X)),
    assertz(rock_field((X, Y)))).


directional_between(Low, High, Value) :-
  between(Low, High, Value).

directional_between(High, Low, Value) :-
  between(Low, High, Value).


%--------------------------------------------------
% Input File Reading
%--------------------------------------------------

assert_input :-
  retractall(rock_path(_)),
  file_lines(input, Lines),
  forall(
    member(Line, Lines),
    assert_rock_path(Line)).

assert_rock_path(Line) :-
  split_string(Line, '->', ' ', CoordinateStrings),
  findall(
    Coordinate,
    ( member(CoordinateString, CoordinateStrings),
      string_to_term(CoordinateString, Coordinate),
      Coordinate \= end_of_file),
    Coordinates),
  assertz(rock_path(Coordinates)).

string_to_term(String, Term) :-
  atom_string(Atom, String),
  atom_to_term(Atom, Term, []).

file_lines(File, Lines) :-
  setup_call_cleanup(
    open(File, read, Stream),
    ( read_string(Stream, _, Content),
      split_string(Content, '\n', '', Lines)),
    close(Stream)).

:- assert_input,
    assert_rock_fields,
    assert_floor.