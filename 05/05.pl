task1(TopCratesString) :-
  play(9000, Stacks),
  top_crates(Stacks, TopCrates),
  string_chars(TopCratesString, TopCrates).

task2(TopCratesString) :-
  play(9001, Stacks),
  top_crates(Stacks, TopCrates),
  string_chars(TopCratesString, TopCrates).

top_crates(Stacks, TopCrates) :-
  findall(
    TopCrate,
    ( between(1, 9, Index),
      member(stack(Index, [TopCrate|_]), Stacks)),
    TopCrates).

play(CraneModel, OutputStacks) :-
  input_stacks(InputStacks),
  play_sequence(CraneModel, InputStacks, OutputStacks).

play_sequence(CraneModel, InputStacks, OutputStacks) :-
  play_sequence(CraneModel, InputStacks, 1, OutputStacks).

play_sequence(CraneModel, InputStacks, Index, NextOutputStacks) :-
  move(Index, NumberOfCrates, SourceStack, DestinationStack),
  play(CraneModel, InputStacks, move(Index, NumberOfCrates, SourceStack, DestinationStack), OutputStacks),
  !,
  NextIndex is Index + 1,
  play_sequence(CraneModel, OutputStacks, NextIndex, NextOutputStacks).

play_sequence(_, Stacks, _, Stacks).

play(CraneModel, InputStacks, move(_, NumberOfCrates, SourceStack, DestinationStack), [stack(SourceStack, NewSourceCrates), stack(DestinationStack, NewDestinationCrates)|OtherStacks]) :-
  member(stack(SourceStack, SourceCrates), InputStacks),
  append(PickCrates, NewSourceCrates, SourceCrates),
  length(PickCrates, NumberOfCrates),
  ( CraneModel = 9001 ->
    DropCrates = PickCrates
  ; reverse(PickCrates, DropCrates)),
  member(stack(DestinationStack, DestinationCrates), InputStacks),
  append(DropCrates, DestinationCrates, NewDestinationCrates),
  exclude(
    =(stack(SourceStack, _)),
    InputStacks,
    OtherStacks1),
  exclude(
    =(stack(DestinationStack, _)),
    OtherStacks1,
    OtherStacks).

input_stacks(InputStacks) :-
  findall(
    stack(Index, Stack),
    starting_stack(Index, Stack),
    InputStacks).

%--------------------------------------------------

assert_inputs :-
  retractall(starting_stack(_, _)),
  retractall(move(_, _, _, _)),
  file_lines(input, Lines),
  append(StartingStackLines, [""|MoveLines], Lines),
  assert_starting_stacks(StartingStackLines),
  assert_moves(MoveLines).

assert_starting_stacks(StartingStackLines) :-
  assert_starting_stack(StartingStackLines, 1).

assert_starting_stack(Lines, Index) :-
  assert_starting_stack(Lines, Index, Stack),
  assertz(starting_stack(Index, Stack)),
  NextIndex is Index + 1,
  assert_starting_stack(Lines, NextIndex),
  !.

assert_starting_stack(_, _).

assert_starting_stack([Line|Lines], Index, Crates) :-
  assert_starting_stack(Lines, Index, NextCrates),
  Position is (Index - 1) * 4 + 1,
  sub_string(Line, Position, 1, _, CrateString),
  atom_string(Crate, CrateString),
  ( Crate = ' ' ->
    Crates = NextCrates
  ; Crates = [Crate|NextCrates]),
  !.

assert_starting_stack([_], _, []).

assert_moves(Lines) :-
    assert_move(1, Lines).

assert_move(_, [""]) :- !.

assert_move(Index, [Line|Lines]) :-
  split_string(Line, ' ', '', ["move", NumberOfCratesString, "from", SourceStackString, "to", DestinationStackString]),
  number_codes(NumberOfCrates, NumberOfCratesString),
  number_codes(SourceStack, SourceStackString),
  number_codes(DestinationStack, DestinationStackString),
  assertz(move(Index, NumberOfCrates, SourceStack, DestinationStack)),
  NextIndex is Index + 1,
  assert_move(NextIndex, Lines).

file_lines(File, Lines) :-
  setup_call_cleanup(
    open(File, read, Stream),
    ( read_string(Stream, _, Content),
      split_string(Content, '\n', '', Lines)),
    close(Stream)).

:- assert_inputs.
