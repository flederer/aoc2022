task1(Count) :-
  aggregate_all(
    count,
    ( group(FirstAssignments, SecondAssignments),
      sub_assignment_group(group(FirstAssignments, SecondAssignments))),
    Count).

task2(Count) :-
  aggregate_all(
    count,
    ( group(FirstAssignments, SecondAssignments),
      overlap_group(group(FirstAssignments, SecondAssignments))),
    Count).

sub_assignment_group(group(FirstAssignments, SecondAssignments)) :-
  sub_assignment(FirstAssignments, SecondAssignments),
  !.

sub_assignment_group(group(FirstAssignments, SecondAssignments)) :-
  sub_assignment(SecondAssignments, FirstAssignments).

sub_assignment(FirstFirstAssignment-LastFirstAssignment, FirstSecondAssignment-LastSecondAssignment) :-
  FirstFirstAssignment =< FirstSecondAssignment,
  LastFirstAssignment >= LastSecondAssignment.

overlap_group(group(FirstAssignments, SecondAssignments)) :-
  overlap_assigment(FirstAssignments, SecondAssignments),
  !.

overlap_group(group(FirstAssignments, SecondAssignments)) :-
  overlap_assigment(SecondAssignments, FirstAssignments),
  !.

overlap_group(group(FirstAssignments, SecondAssignments)) :-
  sub_assignment(FirstAssignments, SecondAssignments).

overlap_assigment(FirstFirstAssignment-LastFirstAssignment, FirstSecondAssignment-_) :-
  FirstFirstAssignment =< FirstSecondAssignment,
  LastFirstAssignment >= FirstSecondAssignment.

%--------------------------------------------------

assert_inputs :-
  retractall(group(_,_)),
  file_lines(input, Lines),
  foreach(
    member(Line, Lines),
    assert_line(Line)).

assert_line(Line) :-
  split_string(Line, ',', '', [FirstAssignmentsString, SecondAssignmentsString]),
  !,
  term_string(FirstAssignments, FirstAssignmentsString),
  term_string(SecondAssignments, SecondAssignmentsString),
  assertz(group(FirstAssignments, SecondAssignments)).

assert_line("").

file_lines(File, Lines) :-
  setup_call_cleanup(
    open(File, read, Stream),
    ( read_string(Stream, _, Content),
      split_string(Content, '\n', '', Lines)),
    close(Stream)).

:- assert_inputs.
