# Advent of Code in SWI Prolog

I want to keep my Prolog skills freshened up. Do not expect perfectly effective and polished Prolog code, here :-).

## How to execute

First, install [SWI-Prolog](https://www.swi-prolog.org/). For Nix users, there is a `shell.nix` file available in the repository.

You can execute the code by entering the day's folder (e.g. `cd 01`) and call SWI-Prolog with the day's source code, e.g.
```
swipl -f 01.pl
```
Each day contains the same two rules `task1(Result)` and `task2(Result)`. Hence, in order to execute the days task, just call in the task in the prompt:
```
?- task1(A).
A = 15337.

?- task1(A).
A = 11696.
```
The code uses the input file form the working directory.
