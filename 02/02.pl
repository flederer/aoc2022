task1(Score) :-
  aggregate_all(
    sum(PlayScore),
    ( play(TheirShape, MyShape),
      play_score(play(TheirShape, MyShape), PlayScore)),
    Score).

task2(Score) :-
  aggregate_all(
    sum(PlayScore),
    ( play2(TheirShape, MyShape),
      play_score(play(TheirShape, MyShape), PlayScore)),
    Score).

shape('A', rock).
shape('X', rock).
shape('B', paper).
shape('Y', paper).
shape('C', scissors).
shape('Z', scissors).

play(TheirShape, MyShape) :-
  input(TheirPlay, MyPlay),
  shape(TheirPlay, TheirShape),
  shape(MyPlay, MyShape).

wins(play(rock, paper)).
wins(play(paper, scissors)).
wins(play(scissors, rock)).

draw(play(Shape, Shape)).

loses(play(Shape2, Shape1)) :-
  wins(play(Shape1, Shape2)).

shape_score(rock, 1).
shape_score(paper, 2).
shape_score(scissors, 3).

outcome_score(Play, 0) :-
  loses(Play),
  !.

outcome_score(Play, 3) :-
  draw(Play),
  !.

outcome_score(Play, 6) :-
  wins(Play).

play_score(Play, Score) :-
  Play = play(_, MyShape),
  shape_score(MyShape, ShapeScore),
  outcome_score(Play, OutcomeScore),
  Score is ShapeScore + OutcomeScore.

result('X', lose).
result('Y', draw).
result('Z', win).

play_result(TheirShape, Result) :-
  input(TheirPlay, MyPlay),
  shape(TheirPlay, TheirShape),
  result(MyPlay, Result).

play2(TheirShape, MyShape) :-
  play_result(TheirShape, win),
  wins(play(TheirShape, MyShape)).

play2(TheirShape, MyShape) :-
  play_result(TheirShape, draw),
  draw(play(TheirShape, MyShape)).

play2(TheirShape, MyShape) :-
  play_result(TheirShape, lose),
  loses(play(TheirShape, MyShape)).

%--------------------------------------------------

assert_inputs :-
  retractall(input(_, _)),
  file_lines(input, Lines),
  forall(
    member(Line, Lines),
    assert_line(Line)).

assert_line(Line) :-
  split_string(Line, ' ', '', [TheirPlayString, MyPlayString]),
  !,
  atom_string(TheirPlay, TheirPlayString),
  atom_string(MyPlay, MyPlayString),
  assertz(input(TheirPlay, MyPlay)).

assert_line("").

file_lines(File, Lines) :-
  setup_call_cleanup(
    open(File, read, Stream),
    ( read_string(Stream, _, Content),
      split_string(Content, '\n', '', Lines)),
    close(Stream)).

:- assert_inputs.
