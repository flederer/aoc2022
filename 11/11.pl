task1(MonkeyBusiness) :-
  play(relief1, 20, Inspections),
  monkey_business(Inspections, MonkeyBusiness).

task2(MonkeyBusiness) :-
  play(relief2, 10000, Inspections),
  monkey_business(Inspections, MonkeyBusiness).

monkey_business(Inspections, MonkeyBusiness) :-
  monkeys(Monkeys),
  findall(
    (Id-InspectionCount),
    ( member(monkey(Id, _, _, _), Monkeys),
      aggregate_all(
        count,
        member(Id, Inspections),
        InspectionCount)),
    InspectionCounts),
  sort(2, @>=, InspectionCounts, [_-MostInspections, _-SecondMostInspections|_]),
  MonkeyBusiness is MostInspections * SecondMostInspections.
  

play(ReliefPredicate, Rounds, Inspections) :-
  monkeys(Monkeys),
  findall(
    Id-Items,
    member(monkey(Id, starting_items(Items), _, _), Monkeys),
    MonkeyItemPairs),
  list_to_assoc(MonkeyItemPairs, MonkeysItems),
  play_rounds(ReliefPredicate, Rounds, Monkeys, MonkeysItems, Inspections).

play_rounds(_, 0, _, _, Inspections) :-
  !,
  empty_assoc(Inspections).

play_rounds(ReliefPredicate, Rounds, Monkeys, MonkeysItems, Inspections) :-
  play_round(ReliefPredicate, Monkeys, MonkeysItems, NextMonkeysItems, RoundInspections),
  NextRounds is Rounds - 1,
  play_rounds(ReliefPredicate, NextRounds, Monkeys, NextMonkeysItems, NextInspections),
  append(RoundInspections, NextInspections, Inspections).

play_round(ReliefPredicate, [monkey(Id, _, Operation, Test)|Monkeys], MonkeysItems, NewMonkeysItems, Inspections) :-
  !,
  get_assoc(Id, MonkeysItems, MonkeyItems),
  inspect_items(ReliefPredicate, Id, MonkeyItems, MonkeysItems, Operation, Test, TmpMonkeysItems, NewInspections),
  put_assoc(Id, TmpMonkeysItems, [], NextMonkeysItems),
  play_round(ReliefPredicate, Monkeys, NextMonkeysItems, NewMonkeysItems, NextInspections),
  append(NewInspections, NextInspections, Inspections).

play_round(_, [], MonkeysItems, MonkeysItems, []).

inspect_items(ReliefPredicate, Id, [Item|Items], MonkeysItems, Operation, Test, NewMonkeysItems, [Id|Inspections]) :-
  !,
  operate(Item, Operation, Level),
  call(ReliefPredicate, Level, BoredLevel),
  test(BoredLevel, Test, NewMonkey),
  add_item_to_assoc_entry(NewMonkey, MonkeysItems, BoredLevel, NextMonkeysItems),
  inspect_items(ReliefPredicate, Id, Items, NextMonkeysItems, Operation, Test, NewMonkeysItems, Inspections).

inspect_items(_, _, [], MonkeysItems, _, _, MonkeysItems, []).


relief1(Level, BoredLevel) :- BoredLevel is floor(Level / 3).

relief2(Level, BoredLevel) :-
  relief_modulo(Modulo),
  BoredLevel is Level mod Modulo.


relief_modulo(Modulo) :-
  monkeys(Monkeys),
  findall(
    TestModulo,
    member(monkey(_, _, _, test(TestModulo, _, _)), Monkeys),
    TestModulos),
  least_common_multiplier(TestModulos, Modulo).


least_common_multiplier([Number], Number) :- !.

least_common_multiplier([Number|Numbers], LeastCommonMultiplier) :-
  least_common_multiplier(Numbers, PreviousLeastCommonMultiplier),
  LeastCommonMultiplier is lcm(Number, PreviousLeastCommonMultiplier).


add_item_to_assoc_entry(Id, InputAssoc, Item, OutputAssoc) :-
  get_assoc(Id, InputAssoc, Entry),
  append(Entry, [Item], NewEntry),
  put_assoc(Id, InputAssoc, NewEntry, OutputAssoc).


operate(Level, operation(Operator, old), NewLevel) :-
  !,
  Operation =.. [Operator, Level, Level],
  NewLevel is Operation.

operate(Level, operation(Operator, Operand), NewLevel) :-
  Operation =.. [Operator, Level, Operand],
  NewLevel is Operation.


test(Level, test(Modulo, TrueMonkey, FalseMonkey), NewMonkey) :-
  (0 is Level mod Modulo ->
    NewMonkey = TrueMonkey
  ; NewMonkey = FalseMonkey).


%--------------------------------------------------
% Definitive Clause Grammars
%--------------------------------------------------

assert_monkeys :-
  retractall(monkeys(_)),
  input(Input),
  phrase(monkeys(Monkeys), Input),
  assertz(monkeys(Monkeys)).

monkeys([Monkey|Monkeys]) -->
  monkey(Monkey), "\n",
  monkeys(Monkeys),
  {!}.

monkeys([Monkey]) -->
  monkey(Monkey).


monkey(monkey(Id, starting_items(StartingItems), operation(Operator, Operan), test(Modulo, TrueMonkey, FalseMonkey))) -->
  header(Id), "\n",
  starting_items(StartingItems), "\n",
  operation(Operator, Operan), "\n",
  test(Modulo, TrueMonkey, FalseMonkey), "\n".


header(Id) --> "Monkey ", integer(Id), ":".


starting_items(Items) --> "  Starting items: ", starting_items_list(Items).

starting_items_list([Item|Items]) --> integer(Item),  ", ", starting_items_list(Items), {!}.

starting_items_list([Item]) --> integer(Item).


operation(Operator, Operand) --> "  Operation: new = old ", [Operator], " ", integer(Operand), {!}.

operation(Operator, old) --> "  Operation: new = old ", [Operator], " old".


test(Modulo, TrueMonkey, FalseMonkey) -->
  "  Test: divisible by ",  integer(Modulo), "\n",
  "    If true: throw to monkey ", integer(TrueMonkey), "\n",
  "    If false: throw to monkey ", integer(FalseMonkey).


integer(Integer) --> integer_digits(Digits), { number_codes(Integer, Digits) }.

integer_digits([Digit|NextDigits]) -->
  dig(Digit),
  integer_digits(NextDigits),
  {!}.

integer_digits([Digit]) --> dig(Digit).

dig(Digit) --> [Digit], { char_type(Digit, digit) }.

%--------------------------------------------------
% Input File Reading
%--------------------------------------------------

assert_input :-
  retractall(input(_)),
  setup_call_cleanup(
    open(input, read, Stream),
    ( read_string(Stream, _, String),
      string_chars(String, Input)),
    close(Stream)),
  assertz(input(Input)).

%--------------------------------------------------
% Automatic Goals
%--------------------------------------------------

:- set_prolog_flag(double_quotes, chars).
:- assert_input.
:- assert_monkeys.

%--------------------------------------------------
% Some Unit Tests
%--------------------------------------------------

:- begin_tests(dcg).

test(digit) :-
  string_chars("9", Chars),
  phrase(dig('9'), Chars).

test(integer_digits) :-
  string_chars("12", Chars),
  phrase(integer_digits(['1','2']), Chars).

test(integer) :-
  string_chars("34", Chars),
  phrase(integer(34), Chars).

test(integer_invalid, [fail]) :-
  string_chars("abc", Chars),
  phrase(integer(_), Chars).

test(header) :-
  string_chars("Monkey 3:", Chars),
  phrase(header(3), Chars).

test(starting_items) :-
  string_chars("  Starting items: 54, 65, 75, 74", Chars),
  phrase(starting_items([54, 65, 75, 74]), Chars).

test(operation) :-
  string_chars("  Operation: new = old + 19", Chars),
  phrase(operation((+), 19), Chars).

test(operation) :-
  string_chars("  Operation: new = old * old", Chars),
  phrase(operation('*', old), Chars).

test(test) :-
  string_chars(
    "  Test: divisible by 23\n    If true: throw to monkey 2\n    If false: throw to monkey 3",
    Chars),
  phrase(test(23, 2, 3), Chars).

:- end_tests(dcg).
