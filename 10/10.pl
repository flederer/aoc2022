task1(TotalSignalStrength) :-
  run(Cycles),
  aggregate_all(
    sum(SignalStrength),
    ( member(Cycle, Cycles),
      Cycle = cycle(Tick, _),
      0 is (Tick - 20) mod 40,
      signal_strengh(Cycle, SignalStrength)
    ),
    TotalSignalStrength).

task2 :-
  run(Cycles),
  split_list(40, Cycles, Lines),
  render_lines(Lines).

run(Cycles) :-
  input(Instructions),
  run(Instructions, 1, 1, Cycles).

run([instruction(noop, []) | Instructions], Tick, Value, [cycle(Tick, Value) | Cycles]) :-
  NextTick is Tick + 1,
  run(Instructions, NextTick, Value, Cycles).

run([instruction(addx, [ValueChange]) | Instructions], Tick, Value, [cycle(Tick, Value), cycle(Tick1, Value) | NextCycles]) :-
  Tick1 is Tick + 1,
  NextTick is Tick + 2,
  NextValue is Value + ValueChange,
  run(Instructions, NextTick, NextValue, NextCycles).

run([], _, _, []).

signal_strengh(cycle(Tick, Value), SignalStrength) :-
  SignalStrength is Tick * Value.

split_list(_, [], []) :- !.

split_list(SubListLength, List, [SubList|SplittedList]) :-
  append(SubList, RestList, List),
  length(SubList, SubListLength),
  !,
  split_list(SubListLength, RestList, SplittedList).

render_lines([Line|Lines]) :-
  !,
  render_line(Line),
  writeln(''),
  render_lines(Lines).

render_lines([]).

render_line(Line) :-
  render_line(0, Line).

render_line(DrawPosition, [Cycle|Cycles]) :-
  !,
  draw_pixel(DrawPosition, Cycle),
  NextDrawPosition is DrawPosition + 1,
  render_line(NextDrawPosition, Cycles).

render_line(_, []).

draw_pixel(DrawPosition, cycle(_, Value)) :-
  MinPosition is Value - 1,
  MaxPosition is Value + 1,
  ((MinPosition =< DrawPosition,
    DrawPosition =< MaxPosition) ->
    Output = '#'
  ; Output = '.'),
  write(Output).

%--------------------------------------------------

assert_input :-
  retractall(input(_)),
  setup_call_cleanup(
    open(input, read, Stream),
    read_string(Stream, _, Content),
    close(Stream)),
  split_string(Content, '\n', '', StringLines),
  findall(
    Instruction,
    ( member(LineString, StringLines),
      line(LineString, Instruction)),
    Instructions),
  assertz(input(Instructions)).

line(Line, instruction(Instruction, Values)) :-
  Line \= "",
  split_string(Line, ' ', '', [InstructionString | ValuesStrings]),
  atom_string(Instruction, InstructionString),
  maplist(
    number_string,
    Values,
    ValuesStrings).

:- assert_input.
