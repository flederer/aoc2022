task1(Sum) :-
  packet_pairs(PacketPairs),
  correct_order_indexes(PacketPairs, Indexes),
  sum_list(Indexes, Sum).

task2(DecoderKey) :-
  FirstDivider = [[2]],
  SecondDivider = [[6]],
  packets(Packets),
  predsort(compare_packets, [FirstDivider, SecondDivider | Packets], SortedPackets),
  list_index(SortedPackets, FirstDivider, FirstDividerPosition),
  list_index(SortedPackets, SecondDivider, SecondDividerPosition),
  DecoderKey is FirstDividerPosition * SecondDividerPosition.


compare_packets(<, Packet1, Packet2) :-
  correct_order(Packet1, Packet2),
  !.

compare_packets(>, _, _).


correct_order_indexes(PacketPairs, Indexes) :- correct_order_indexes(PacketPairs, 1, Indexes).

correct_order_indexes([packet_pair(SamllerTerm, GreaterTerm)|PacketPairs], Index, [Index|Indexes]) :-
  correct_order(SamllerTerm, GreaterTerm),
  !,
  NextIndex is Index + 1,
  correct_order_indexes(PacketPairs, NextIndex, Indexes).

correct_order_indexes([_|PacketPairs], Index, Indexes) :-
  !,
  NextIndex is Index + 1,
  correct_order_indexes(PacketPairs, NextIndex, Indexes).

correct_order_indexes([], _, []).


correct_order([SmallerTerm|SmallerRest], [GreaterTerm|GreaterRest]) :-
  term_list(SmallerTerm, List),
  term_list(GreaterTerm, List),
  !,
  correct_order(SmallerRest, GreaterRest).

correct_order([SmallerTerm|_], [GreaterTerm|_]) :-
  term_list(SmallerTerm, [SmallerElement]),
  term_list(GreaterTerm, [GreaterElement]),
  integer(SmallerElement),
  integer(GreaterElement),
  !,
  SmallerElement < GreaterElement.

correct_order([], _) :- !.

correct_order([SmallerTerm|_], [GreaterTerm|_]) :-
  term_list(SmallerTerm, SmallerList),
  term_list(GreaterTerm, GreaterList),
  correct_order(SmallerList, GreaterList).


term_list(Term, [Term]) :-
  integer(Term),
  !.

term_list(Term, Term).


packets(Packets) :-
  packet_pairs(PacketPairs),
  packets(PacketPairs, Packets).

packets([packet_pair(Packet1, Packet2) | PacketPairs], [Packet1, Packet2 | Packets]) :-
  packets(PacketPairs, Packets).

packets([], []).


list_index(List, Element, ResultIndex) :-
  list_index(List, Element, 1, ResultIndex).

list_index([Element|_], Element, Index, Index) :- !.

list_index([_|List], Element, MyIndex, ResultIndex) :-
  NextIndex is MyIndex + 1,
  list_index(List, Element, NextIndex, ResultIndex).


%--------------------------------------------------
% Input File Reading
%--------------------------------------------------

assert_input :-
  retractall(packet_pairs(_)),
  file_lines(input, Lines),
  line_pairs(Lines, PacketPairs),
  assertz(packet_pairs(PacketPairs)).

line_pairs([Line1, Line2, "" | Lines], [packet_pair(Term1, Term2) | PacketPairs]) :-
  !,
  string_to_term(Line1, Term1),
  string_to_term(Line2, Term2),
  line_pairs(Lines, PacketPairs).

line_pairs([], []).

string_to_term(String, Term) :-
  atom_string(Atom, String),
  atom_to_term(Atom, Term, []).

file_lines(File, Lines) :-
  setup_call_cleanup(
    open(File, read, Stream),
    ( read_string(Stream, _, Content),
      split_string(Content, '\n', '', Lines)),
    close(Stream)).

:- assert_input.
