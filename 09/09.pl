task1(NumberOfTailPositions) :-
  play(1, Tails),
  list_to_set(Tails, TailsSet),
  length(TailsSet, NumberOfTailPositions).

task2(NumberOfTailPositions) :-
  play(9, Tails),
  list_to_set(Tails, TailsSet),
  length(TailsSet, NumberOfTailPositions).

play(NumberOfKnots, Tails) :-
  input(Input),
  fill_list((0, 0), NumberOfKnots, Knots),
  play(Input, (0, 0), Knots, Tails).

play([move(Direction, 1) | Moves], HeadPosition, Knots, [NewTail|NextTails]) :-
  !,
  move_head(Direction, HeadPosition, NewHead),
  move_knots(NewHead, Knots, NewKnots, NewTail),
  play(Moves, NewHead, NewKnots, NextTails).

play([move(Direction, Distance) | Moves], HeadPosition, Knots, [NewTail|NextTails]) :-
  !,
  move_head(Direction, HeadPosition, NewHead),
  move_knots(NewHead, Knots, NewKnots, NewTail),
  NextDistance is Distance - 1,
  play([move(Direction, NextDistance) | Moves], NewHead, NewKnots, NextTails).

play([], _, _, []).

move_head('U', (Row, Column), (NewRow, Column)) :-
  NewRow is Row - 1,
  !.

move_head('D', (Row, Column), (NewRow, Column)) :-
  NewRow is Row + 1,
  !.

move_head('L', (Row, Column), (Row, NewColumn)) :-
  NewColumn is Column - 1,
  !.

move_head('R', (Row, Column), (Row, NewColumn)) :-
  NewColumn is Column + 1.

move_knots(Knot, [], [], Knot) :- !.

move_knots(Head, [Knot|Knots], [NewKnot|NewKnots], Tail) :-
  move_knot(Head, Knot, NewKnot),
  move_knots(NewKnot, Knots, NewKnots, Tail).

move_knot(A, A, A) :- !.

move_knot(PreviousKnot, Knot, Knot) :-
  neighbors(Knot, KnotNeighbors),
  memberchk(PreviousKnot, KnotNeighbors),
  !.

move_knot(PreviousKnot, Knot, NewKnot) :-
  neighbors(Knot, KnotNeighbors),
  direct_neighbors(PreviousKnot, PreviousKnotNeighbors),
  intersection(KnotNeighbors, PreviousKnotNeighbors, [NewKnot]),
  !.

move_knot(PreviousKnot, Knot, NewKnot) :-
  neighbors(Knot, KnotNeighbors),
  neighbors(PreviousKnot, PreviousKnotNeighbors),
  intersection(KnotNeighbors, PreviousKnotNeighbors, [NewKnot]),
  !.

neighbors((Row, Column), Neighbors) :-
  Up is Row - 1,
  Down is Row + 1,
  Left is Column - 1,
  Right is Column + 1,
  findall(
      (NeighborRow, NeighborColumn),
      ( between(Up, Down, NeighborRow),
        between(Left, Right, NeighborColumn)),
      Neighbors).

direct_neighbors((Row, Column), Neighbors) :-
  Up is Row - 1,
  Down is Row + 1,
  Left is Column - 1,
  Right is Column + 1,
  findall(
    (NeighborRow, Column),
    between(Up, Down, NeighborRow),
    RowNeighbors),
  findall(
    (Row, NeighborColumn),
    between(Left, Right, NeighborColumn),
    ColumnNeighbors),
  append(RowNeighbors, ColumnNeighbors, Neighbors).

fill_list(_, 0, []) :- !.

fill_list(Element, Length, [Element|List]) :-
  NextLength is Length - 1,
  fill_list(Element, NextLength, List).

%--------------------------------------------------

assert_input :-
  retractall(input(_)),
  setup_call_cleanup(
    open(input, read, Stream),
    read_string(Stream, _, Content),
    close(Stream)),
  split_string(Content, '\n', '', StringLines),
  findall(
    move(Direction, Distance),
    ( member(LineString, StringLines),
      line(LineString, Direction, Distance)),
    Moves),
  assertz(input(Moves)).

line(Line, Direction, Distance) :-
  split_string(Line, ' ', '', [DirectionString, DistanceString]),
  atom_string(Direction, DirectionString),
  number_string(Distance, DistanceString).

:- assert_input.
